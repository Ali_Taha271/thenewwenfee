-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 25, 2020 at 04:25 PM
-- Server version: 5.6.49-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wenfedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(2, NULL, 1, 'Category 2', 'category-2', '2020-11-23 22:10:01', '2020-11-23 22:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `cook_books`
--

CREATE TABLE `cook_books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cook_books`
--

INSERT INTO `cook_books` (`id`, `title`, `author`, `description`, `image`, `price`, `created_at`, `updated_at`) VALUES
(1, 'THE COMPLETE MIDDLE EAST COOKBOOK', 'Tess Mallos', '<p>..</p>', 'cook-books/December2020/ISYjCauBb1khPllny1Mh.jpg', 15, '2020-12-05 13:22:52', '2020-12-05 13:22:52'),
(2, 'THE LEBANESE COOKBOOK', 'Dawn, Elaine & Selwa Anthony', '<p>..</p>', 'cook-books/December2020/dxtcHN14If5bANAniEpH.jpg', 18, '2020-12-05 13:23:53', '2020-12-05 13:23:53'),
(3, 'NORTH AFRICAN COOKING', 'Tess Mallos', '<p>..</p>', 'cook-books/December2020/1Js59ssMHCRNHb4rIugr.jpg', 12, '2020-12-05 13:24:35', '2020-12-05 13:24:35'),
(4, 'TURKISH COOKING', 'Tess Mallos', '<p>..</p>', 'cook-books/December2020/EgAOrTf7mDhmVCCVpVuq.jpg', 12, '2020-12-05 13:25:17', '2020-12-05 13:25:17');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":6},\"validation\":{\"rule\":\"required\"}}', 2),
(58, 7, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:product_categories,slug\"},\"display\":{\"width\":6}}', 3),
(59, 7, 'order', 'number', 'Order', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":6},\"validation\":{\"rule\":\"required\"}}', 5),
(60, 7, 'is_homepage', 'checkbox', 'Is Homepage', 1, 1, 1, 1, 1, 1, '{\"on\":\"Yes\",\"off\":\"No\"}', 6),
(61, 7, 'is_more', 'checkbox', 'Is More', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":6},\"on\":\"Yes\",\"off\":\"No\"}', 7),
(62, 7, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 8),
(63, 7, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 9),
(64, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(65, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(66, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":6},\"validation\":{\"rule\":\"required\"}}', 2),
(67, 8, 'code', 'text', 'Code', 0, 1, 1, 1, 1, 1, '{}', 4),
(68, 8, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"2500\",\"height\":null},\"quality\":\"70%\",\"upsize\":false,\"thumbnails\":[{\"name\":\"cropped\",\"crop\":{\"width\":\"640\",\"height\":\"640\"}}]}', 7),
(69, 8, 'gallery', 'multiple_images', 'Gallery', 0, 0, 1, 1, 1, 1, '{\"resize\":{\"width\":\"2500\",\"height\":null},\"quality\":\"70%\",\"upsize\":false,\"thumbnails\":[{\"name\":\"cropped\",\"crop\":{\"width\":\"850\",\"height\":\"520\"}}]}', 8),
(70, 8, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 9),
(71, 8, 'size', 'text', 'Size', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 10),
(72, 8, 'size_unit_id', 'text', 'Size Unit Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 12),
(73, 8, 'is_active', 'checkbox', 'Is Active', 1, 1, 1, 1, 1, 1, '{\"on\":\"Yes\",\"off\":\"No\",\"display\":{\"width\":6}}', 15),
(74, 8, 'is_offer', 'checkbox', 'Is Offer', 0, 1, 1, 1, 1, 1, '{\"on\":\"Yes\",\"off\":\"No\",\"display\":{\"width\":6}}', 13),
(75, 8, 'is_large', 'checkbox', 'Is Large', 0, 1, 1, 1, 1, 1, '{\"on\":\"Yes\",\"off\":\"No\",\"display\":{\"width\":6}}', 16),
(76, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 17),
(77, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 18),
(78, 8, 'product_belongsto_product_category_relationship', 'relationship', 'Category', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"product_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(79, 8, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:products,slug\"},\"display\":{\"width\":6}}', 3),
(80, 8, 'product_category_id', 'text', 'Product Category Id', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 19),
(81, 8, 'product_belongsto_size_unit_relationship', 'relationship', 'unit', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\SizeUnit\",\"table\":\"size_units\",\"type\":\"belongsTo\",\"column\":\"size_unit_id\",\"key\":\"id\",\"label\":\"unit\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(82, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(83, 9, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 2),
(84, 9, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 3),
(85, 9, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"2500\",\"height\":null},\"quality\":\"70%\",\"upsize\":false,\"validation\":{\"rule\":\"required\"}}', 4),
(86, 9, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 5),
(87, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(88, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(89, 10, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":6},\"validation\":{\"rule\":\"required\"}}', 2),
(90, 10, 'author', 'text', 'Author', 1, 1, 1, 1, 1, 1, '{\"display\":{\"width\":6},\"validation\":{\"rule\":\"required\"}}', 3),
(91, 10, 'description', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 4),
(92, 10, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"2500\",\"height\":null},\"quality\":\"70%\",\"upsize\":false,\"thumbnails\":[{\"name\":\"cropped\",\"crop\":{\"width\":\"800\",\"height\":\"660\"}}],\"validation\":{\"rule\":\"required\"}}', 5),
(93, 10, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 6),
(94, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 7),
(95, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(96, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(97, 11, 'unit', 'text', 'Unit', 1, 1, 1, 1, 1, 1, '{}', 2),
(98, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 3),
(99, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(100, 7, 'product_category_belongsto_product_category_relationship', 'relationship', 'Parent', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\ProductCategory\",\"table\":\"product_categories\",\"type\":\"belongsTo\",\"column\":\"parent_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(101, 7, 'parent_id', 'text', 'Parent Id', 0, 1, 1, 1, 1, 1, '{}', 11),
(102, 8, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\"}}', 6),
(103, 8, 'discount', 'number', 'Discount', 0, 0, 1, 1, 1, 1, '{\"display\":{\"width\":6}}', 14);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(7, 'product_categories', 'product-categories', 'Product Category', 'Product Categories', NULL, 'App\\Models\\ProductCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-11-23 22:37:45', '2020-11-29 03:28:54'),
(8, 'products', 'products', 'Product', 'Products', NULL, 'App\\Models\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-11-23 22:41:02', '2020-12-02 06:30:33'),
(9, 'slides', 'slides', 'Slide', 'Slides', NULL, 'App\\Models\\Slide', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-11-23 22:51:39', '2020-11-29 03:34:40'),
(10, 'cook_books', 'cook-books', 'Cook Book', 'Cook Books', NULL, 'App\\Models\\CookBook', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-11-23 22:55:33', '2020-11-29 03:27:35'),
(11, 'size_units', 'size-units', 'Size Unit', 'Size Units', NULL, 'App\\Models\\SizeUnit', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-11-23 22:59:51', '2020-11-23 22:59:51');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-11-23 22:10:01', '2020-11-23 22:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-11-23 22:10:01', '2020-11-23 22:10:01', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2020-11-23 22:10:01', '2020-11-23 22:57:04', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2020-11-23 22:10:01', '2020-11-23 22:10:01', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2020-11-23 22:10:01', '2020-11-23 22:10:01', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 6, '2020-11-23 22:10:01', '2020-11-23 22:57:04', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-11-23 22:10:01', '2020-11-23 22:37:55', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-11-23 22:10:01', '2020-11-23 22:37:55', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-11-23 22:10:01', '2020-11-23 22:37:55', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-11-23 22:10:01', '2020-11-23 22:37:55', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 7, '2020-11-23 22:10:01', '2020-11-23 22:57:04', 'voyager.settings.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-11-23 22:10:02', '2020-11-23 22:37:55', 'voyager.hooks', NULL),
(15, 1, 'Website Contents', '', '_self', 'voyager-window-list', '#000000', NULL, 4, '2020-11-23 22:35:10', '2020-11-23 22:37:59', NULL, ''),
(17, 1, 'Product Categories', '', '_self', 'voyager-categories', '#000000', 15, 1, '2020-11-23 22:37:45', '2020-11-23 22:38:16', 'voyager.product-categories.index', 'null'),
(18, 1, 'Products', '', '_self', 'voyager-hotdog', '#000000', 15, 2, '2020-11-23 22:41:02', '2020-11-23 22:50:08', 'voyager.products.index', 'null'),
(19, 1, 'Slides', '', '_self', 'voyager-photos', '#000000', 15, 3, '2020-11-23 22:51:39', '2020-11-23 22:53:59', 'voyager.slides.index', 'null'),
(20, 1, 'Cook Books', '', '_self', 'voyager-book', '#000000', 15, 4, '2020-11-23 22:55:33', '2020-11-23 22:57:23', 'voyager.cook-books.index', 'null'),
(21, 1, 'Size Units', '', '_self', 'voyager-barbell', '#000000', 15, 5, '2020-11-23 22:59:51', '2020-11-23 23:00:20', 'voyager.size-units.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2019_08_19_000000_create_failed_jobs_table', 1),
(28, '2020_10_16_084923_create_products_table', 1),
(29, '2020_10_16_084942_create_product_categories_table', 1),
(30, '2020_10_16_085002_create_size_units_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2020-11-23 22:10:01', '2020-11-23 22:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(2, 'browse_bread', NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(3, 'browse_database', NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(4, 'browse_media', NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(5, 'browse_compass', NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(6, 'browse_menus', 'menus', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(7, 'read_menus', 'menus', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(8, 'edit_menus', 'menus', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(9, 'add_menus', 'menus', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(10, 'delete_menus', 'menus', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(11, 'browse_roles', 'roles', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(12, 'read_roles', 'roles', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(13, 'edit_roles', 'roles', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(14, 'add_roles', 'roles', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(15, 'delete_roles', 'roles', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(16, 'browse_users', 'users', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(17, 'read_users', 'users', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(18, 'edit_users', 'users', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(19, 'add_users', 'users', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(20, 'delete_users', 'users', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(21, 'browse_settings', 'settings', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(22, 'read_settings', 'settings', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(23, 'edit_settings', 'settings', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(24, 'add_settings', 'settings', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(25, 'delete_settings', 'settings', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(26, 'browse_categories', 'categories', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(27, 'read_categories', 'categories', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(28, 'edit_categories', 'categories', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(29, 'add_categories', 'categories', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(30, 'delete_categories', 'categories', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(31, 'browse_posts', 'posts', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(32, 'read_posts', 'posts', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(33, 'edit_posts', 'posts', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(34, 'add_posts', 'posts', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(35, 'delete_posts', 'posts', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(36, 'browse_pages', 'pages', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(37, 'read_pages', 'pages', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(38, 'edit_pages', 'pages', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(39, 'add_pages', 'pages', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(40, 'delete_pages', 'pages', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(41, 'browse_hooks', NULL, '2020-11-23 22:10:02', '2020-11-23 22:10:02'),
(42, 'browse_product_categories', 'product_categories', '2020-11-23 22:37:45', '2020-11-23 22:37:45'),
(43, 'read_product_categories', 'product_categories', '2020-11-23 22:37:45', '2020-11-23 22:37:45'),
(44, 'edit_product_categories', 'product_categories', '2020-11-23 22:37:45', '2020-11-23 22:37:45'),
(45, 'add_product_categories', 'product_categories', '2020-11-23 22:37:45', '2020-11-23 22:37:45'),
(46, 'delete_product_categories', 'product_categories', '2020-11-23 22:37:45', '2020-11-23 22:37:45'),
(47, 'browse_products', 'products', '2020-11-23 22:41:02', '2020-11-23 22:41:02'),
(48, 'read_products', 'products', '2020-11-23 22:41:02', '2020-11-23 22:41:02'),
(49, 'edit_products', 'products', '2020-11-23 22:41:02', '2020-11-23 22:41:02'),
(50, 'add_products', 'products', '2020-11-23 22:41:02', '2020-11-23 22:41:02'),
(51, 'delete_products', 'products', '2020-11-23 22:41:02', '2020-11-23 22:41:02'),
(52, 'browse_slides', 'slides', '2020-11-23 22:51:39', '2020-11-23 22:51:39'),
(53, 'read_slides', 'slides', '2020-11-23 22:51:39', '2020-11-23 22:51:39'),
(54, 'edit_slides', 'slides', '2020-11-23 22:51:39', '2020-11-23 22:51:39'),
(55, 'add_slides', 'slides', '2020-11-23 22:51:39', '2020-11-23 22:51:39'),
(56, 'delete_slides', 'slides', '2020-11-23 22:51:39', '2020-11-23 22:51:39'),
(57, 'browse_cook_books', 'cook_books', '2020-11-23 22:55:33', '2020-11-23 22:55:33'),
(58, 'read_cook_books', 'cook_books', '2020-11-23 22:55:33', '2020-11-23 22:55:33'),
(59, 'edit_cook_books', 'cook_books', '2020-11-23 22:55:33', '2020-11-23 22:55:33'),
(60, 'add_cook_books', 'cook_books', '2020-11-23 22:55:33', '2020-11-23 22:55:33'),
(61, 'delete_cook_books', 'cook_books', '2020-11-23 22:55:33', '2020-11-23 22:55:33'),
(62, 'browse_size_units', 'size_units', '2020-11-23 22:59:51', '2020-11-23 22:59:51'),
(63, 'read_size_units', 'size_units', '2020-11-23 22:59:51', '2020-11-23 22:59:51'),
(64, 'edit_size_units', 'size_units', '2020-11-23 22:59:51', '2020-11-23 22:59:51'),
(65, 'add_size_units', 'size_units', '2020-11-23 22:59:51', '2020-11-23 22:59:51'),
(66, 'delete_size_units', 'size_units', '2020-11-23 22:59:51', '2020-11-23 22:59:51');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2020-11-23 22:10:01', '2020-11-23 22:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gallery` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(8,2) NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_unit_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_offer` tinyint(1) DEFAULT NULL,
  `is_large` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_category_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `code`, `image`, `gallery`, `price`, `size`, `size_unit_id`, `is_active`, `is_offer`, `is_large`, `created_at`, `updated_at`, `slug`, `product_category_id`, `body`, `discount`) VALUES
(5, 'Italian Eggplant', 'EGG003', 'products/December2020/HseuKyqGMmXQ9JXnGMyP.jpg', '[\"products\\/December2020\\/wVFy84WA6WnKaoVxiaQm.jpg\",\"products\\/December2020\\/99Y3iRSzah8ap7bJRCsP.jpg\",\"products\\/December2020\\/rPDXnGz23bRjbmVNHPRj.jpg\",\"products\\/December2020\\/HCZ779dtnNrnMBxiXXip.jpg\"]', 2.40, '1', 1, 1, 0, 0, '2020-12-01 08:50:48', '2020-12-01 08:50:48', 'italian-eggplant', 6, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here,</span></p>', NULL),
(6, 'Corn', 'corn001', 'products/December2020/oOVuaD1eKDVcDAqkW5GF.jpg', '[\"products\\/December2020\\/CKoRZJCLFaNgxIW450Vm.jpg\",\"products\\/December2020\\/cAqFP18g0qkpKVO9vqmT.jpg\"]', 3.50, '2', 1, 1, 1, 0, '2020-12-01 09:01:37', '2020-12-02 06:32:12', 'corn', 6, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here,</span></p>', 5),
(7, 'Leaf Lettuce', 'le001', 'products/December2020/A8Hkovht1sLkRjaDFoOn.jpg', '[\"products\\/December2020\\/15DQOuwEH74yHDg9sP5d.jpg\",\"products\\/December2020\\/iCnRMNP6jRL9FUUz0Bll.jpg\",\"products\\/December2020\\/M94DkrwDbbkR2dIMSYHO.jpg\",\"products\\/December2020\\/KQlDq8uqrml9gyaz2gtU.jpg\"]', 1.10, '1', 1, 1, 0, 0, '2020-12-01 09:05:48', '2020-12-01 09:05:48', 'leaf-lettuce', 6, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here,</span></p>', NULL),
(8, 'Mint', 'min001', 'products/December2020/e6DMMXoYc7N1iAbZGJOL.jpg', '[\"products\\/December2020\\/5nyEje1AZqokynh6JEOI.jpg\",\"products\\/December2020\\/iJfzWAWULa1adTTWVQUD.jpg\",\"products\\/December2020\\/WeM5TmLuMQpR7ZwSau9p.jpg\",\"products\\/December2020\\/2LaU3Z725Xr0x2uxxixY.jpg\"]', 0.70, '1', 1, 1, 0, 0, '2020-12-01 09:09:15', '2020-12-01 09:09:15', 'mint', 6, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here,</span></p>', NULL),
(9, 'Tomato', 'tmt005', 'products/December2020/IM5fgrdjJgEIPyG4oHlB.jpg', '[\"products\\/December2020\\/kk4kDz7cRU3XVelN2RzS.jpg\",\"products\\/December2020\\/dloNIJtmTUrRsoh1rjR4.jpg\"]', 2.50, '1', 1, 1, 1, 0, '2020-12-05 12:42:36', '2020-12-05 12:42:36', 'tomato', 6, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; font-size: medium; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here</span></p>', 20),
(10, 'Banana', 'ba001', 'products/December2020/UE8HpsaoNF7CuatMizg7.jpg', '[\"products\\/December2020\\/2qpHMnaM9S535NJBbAp5.jpg\",\"products\\/December2020\\/ZyRfyPnxIBsTQrRIQRsE.jpg\"]', 4.60, '1', 1, 1, 0, 0, '2020-12-05 13:07:18', '2020-12-05 13:07:18', 'banana', 10, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; font-size: medium; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here</span></p>', NULL),
(11, 'Apple', 'ap001', 'products/December2020/s6WFlFrkjSVEubkj1cG2.jpg', '[\"products\\/December2020\\/OwyB05Cbu1PDjC26a2rt.jpg\",\"products\\/December2020\\/UoScs2ERwYBwS8qlLRjt.jpg\"]', 1.50, '1', 1, 1, 1, 0, '2020-12-05 13:08:32', '2020-12-05 13:08:32', 'apple', 10, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; font-size: medium; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here</span></p>', 10),
(12, 'Orange', 'org005', 'products/December2020/L5hRDaMWbhO9FjxJq8oH.jpg', '[\"products\\/December2020\\/DHnhhBVxUaHy52qTl70Q.jpg\",\"products\\/December2020\\/ilZyitN7YS8XxppeKYp7.jpg\"]', 0.70, '1', 1, 1, 0, 0, '2020-12-05 13:09:56', '2020-12-05 13:09:56', 'orange', 10, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; font-size: medium; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here</span></p>', NULL),
(13, 'Kiwi', 'ki006', 'products/December2020/V5y1zJ8TFZf70t9ke85Z.jpg', '[\"products\\/December2020\\/a7rv9tcZ9vf9vkEfKJO2.jpg\",\"products\\/December2020\\/TLFH8CdtABqhYJSsmCeo.jpg\"]', 6.20, '1', 1, 1, 1, 0, '2020-12-05 13:11:12', '2020-12-05 13:11:12', 'kiwi', 10, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; font-size: medium; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here</span></p>', 40),
(14, 'Watermelon', 'wm565', 'products/December2020/SgulNQT6eTfrPijRNW3y.jpg', '[\"products\\/December2020\\/VBXmaXF63sEfiVLsgFi7.jpg\",\"products\\/December2020\\/IYteHE1kcj1YZ1mNgpDY.jpg\"]', 8.50, '1', 1, 1, 0, 0, '2020-12-05 13:12:37', '2020-12-05 13:12:37', 'watermelon', 10, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; font-size: medium; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here</span></p>', NULL),
(15, 'strawberry', 'st465', 'products/December2020/CxGzLcfvkOG6mxZjgFhB.jpg', '[\"products\\/December2020\\/QPzL0TfpanGRm0bN5alB.jpg\",\"products\\/December2020\\/V1p8JMrIZtYqd09Rwkhx.jpg\"]', 3.80, '1', 1, 1, 0, 0, '2020-12-05 13:18:06', '2020-12-05 13:18:06', 'strawberry', 10, '<p><span style=\"color: #000000; font-family: \'Open Sans\', Arial, sans-serif; font-size: medium; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here</span></p>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `is_homepage` tinyint(4) NOT NULL,
  `is_more` tinyint(4) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `order`, `is_homepage`, `is_more`, `image`, `created_at`, `updated_at`, `parent_id`) VALUES
(5, 'Vegetables', 'vegetables', 1, 0, 0, NULL, '2020-12-01 08:11:41', '2020-12-01 08:11:41', NULL),
(6, 'Fresh Vegetables', 'fresh-vegetables', 1, 1, 0, NULL, '2020-12-01 08:12:38', '2020-12-01 08:12:38', 5),
(7, 'Frozen Vegetables', 'frozen-vegetables', 2, 0, 0, NULL, '2020-12-01 08:13:11', '2020-12-01 08:13:11', 5),
(8, 'Refrigerator Vegetables', 'refrigerator-vegetables', 3, 0, 0, NULL, '2020-12-01 08:14:01', '2020-12-01 08:14:01', 5),
(9, 'Fruits', 'fruits', 2, 0, 0, NULL, '2020-12-01 08:15:23', '2020-12-01 08:15:23', NULL),
(10, 'Fresh Fruits', 'fresh-fruits', 1, 1, 0, NULL, '2020-12-01 08:15:40', '2020-12-05 13:13:08', 9),
(11, 'Refrigerator Fruits', 'refrigerator-fruits', 2, 0, 0, NULL, '2020-12-01 08:17:36', '2020-12-01 08:17:36', 9),
(12, 'Gourmet Food', 'gourmet-food', 0, 0, 0, NULL, '2020-12-01 08:21:57', '2020-12-01 08:21:57', NULL),
(13, 'Breakfast & Spreads', 'breakfast-and-spreads', 1, 0, 0, NULL, '2020-12-01 08:23:01', '2020-12-01 08:23:01', 12),
(14, 'Pickles and Olives', 'pickles-and-olives', 2, 0, 0, NULL, '2020-12-01 08:24:33', '2020-12-01 08:24:33', 12),
(15, 'Pie and Pastry Dough', 'pie-and-pastry-dough', 3, 0, 0, NULL, '2020-12-01 08:25:11', '2020-12-01 08:25:11', 12),
(16, 'Bakery', 'bakery', 4, 0, 0, NULL, '2020-12-01 08:29:30', '2020-12-01 08:29:30', NULL),
(17, 'Pita Bread', 'pita-bread', 1, 0, 0, NULL, '2020-12-01 08:30:14', '2020-12-01 08:30:14', 16),
(18, 'Bread', 'bread', 2, 0, 0, NULL, '2020-12-01 08:30:46', '2020-12-01 08:30:46', 16),
(19, 'Pie', 'pie', 3, 0, 0, NULL, '2020-12-01 08:31:04', '2020-12-01 08:31:04', 16),
(20, 'Bagel', 'bagel', 4, 0, 0, NULL, '2020-12-01 08:31:31', '2020-12-01 08:31:31', 16),
(21, 'Dairy Products', 'dairy-products', 5, 0, 0, NULL, '2020-12-01 08:33:03', '2020-12-01 08:33:03', NULL),
(22, 'Meat and Seafood', 'meat-and-seafood', 6, 0, 0, NULL, '2020-12-01 08:33:36', '2020-12-01 08:33:36', NULL),
(23, 'Special Offers', 'special-offers', 8, 0, 0, NULL, '2020-12-01 08:35:57', '2020-12-01 08:35:57', NULL),
(24, 'sweets', 'sweets', 25, 0, 1, NULL, '2020-12-10 01:15:11', '2020-12-10 01:15:11', NULL),
(26, 'Rice and Grains', 'rice-and-grains', 2, 0, 0, NULL, '2020-12-10 21:06:21', '2020-12-10 21:06:21', 12),
(27, 'Beverages', 'beverages', 6, 0, 0, NULL, '2020-12-10 21:07:35', '2020-12-10 21:07:35', 12),
(28, 'Canned Foods', 'canned-foods', 2, 0, 0, NULL, '2020-12-10 21:10:00', '2020-12-10 21:10:00', 12),
(29, 'Eggplants', 'eggplants', 1, 0, 0, NULL, '2020-12-10 21:10:23', '2020-12-10 21:10:23', 28),
(30, 'Medical Supplies', 'medical-supplies', 4, 0, 0, NULL, '2020-12-10 21:11:58', '2020-12-10 21:11:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(2, 'user', 'Normal User', '2020-11-23 22:10:01', '2020-11-23 22:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Wenfee', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Wenfee', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Wenfee dashboard.', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `size_units`
--

CREATE TABLE `size_units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `size_units`
--

INSERT INTO `size_units` (`id`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'Kg', '2020-11-23 23:02:43', '2020-11-23 23:02:43'),
(2, 'lbs', '2020-11-23 23:02:52', '2020-11-23 23:02:52');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'You Can Write Any Title Here', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here,', 'slides/December2020/KqqUaQz5izbXWPyPJSRt.jpg', '2020-11-24 04:15:03', '2020-12-01 08:43:55'),
(2, 'Your Title Will Appear here', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,', 'slides/December2020/cgt3rud0NAY24KyaTw9S.jpg', '2020-11-24 04:15:56', '2020-12-01 08:42:45');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-11-23 22:10:01', '2020-11-23 22:10:01'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-11-23 22:10:02', '2020-11-23 22:10:02'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-11-23 22:10:02', '2020-11-23 22:10:02'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-11-23 22:10:02', '2020-11-23 22:10:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$fI22QpL0ojX8qa1W.c3Iv.uDoa6HKfbDdwbuC6I1a04y.yAmMsofG', 'r57mhe8zzMNoauNsJAvcXZX6HMgLC74W6MvNQQwfi7ALABRliBkkr7VRmjsF', NULL, '2020-11-23 22:10:01', '2020-11-23 22:10:01');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `cook_books`
--
ALTER TABLE `cook_books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_categories_parent_id_index` (`parent_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `size_units`
--
ALTER TABLE `size_units`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cook_books`
--
ALTER TABLE `cook_books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `size_units`
--
ALTER TABLE `size_units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
