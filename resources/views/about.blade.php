@extends('layouts.app')

@section('content')
<center>
 
    <div style="margin: 15%">
        <center><h1>WENFEE</h1></center>

Welcome to Wenfee.com
We have specialized in gourmet foods since 1965, and we aim to offer our customers a variety of the latest Middle Eastern Products. We’ve come a long way, so we know exactly which direction to take when supplying you with high quality yet budget friendly products. We offer all of this while providing excellent customer service and friendly support.

The interests of our customers are always the top priority for us, so we hope you will enjoy our products as much as we enjoy making them available to you.

We know you have a choice.
Thank You for Choosing to Shop with us.
We Appreciate your Business.
    </div>
</center>

@endsection