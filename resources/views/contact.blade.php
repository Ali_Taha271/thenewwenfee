@extends('layouts.app')

@section('content')
<center>
 
    <div style="margin: 10%">
        <center><h1>WENFEE</h1></center>
        <table>
 
            <tbody><tr>
                <td colspan="2" style="padding-bottom: 20px;">
                    <a href="http://wenfee.com" class="wenfee-link">wenfee.com</a><br>
                    <span class="address">7184 Troy Hill Drive SUIT C  Elkridge MD 21075 USA</span>
                    <br><br>
                     <div class="google-maps">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4436.563288335262!2d-76.74920380408464!3d39.19409239099808!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b7e1913f4b4fdb%3A0x18a1a674d9c0d7ad!2s7184%20Troy%20Hill%20Dr%2C%20Elkridge%2C%20MD%2021075%2C%20USA!5e0!3m2!1sen!2sro!4v1573996589257!5m2!1sen!2sro" frameborder="0" style="border:0; width:100%" allowfullscreen=""></iframe>
                    </div>
                </td>
            </tr>
            <tr>
                <td>General Information:</td>
                <td><a href="mailto:info@wenfee.com">info@wenfee.com</a></td>
            </tr>
            <tr>
                <td>Products Information:</td>
                <td><a href="mailto:products@wenfee.com">products@wenfee.com</a></td>
            </tr>
            <tr>
                <td>Suggestions Information:</td>
                <td><a href="mailto:suggestions@wenfee.com">suggestions@wenfee.com</a></td>
            </tr>
            <tr>
                <td>Checking Your Order:</td>
                <td><a href="mailto:eorders@wenfee.com">eorders@wenfee.com</a></td>
            </tr>
            <tr>
                <td>Phone:</td>
                <td>(301) 202-7905 or (410) 379-2267</td>
            </tr>
            <tr>
                <td>Fax:</td>
                <td>(240) 337-6468</td>
            </tr>
        </tbody></table>
    </div>
</center>

@endsection