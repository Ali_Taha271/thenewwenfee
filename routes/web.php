<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',                       [App\Http\Controllers\PageController::class, 'landing'])->name('landing-page');
Route::get('/about',                    [App\Http\Controllers\PageController::class, 'about'])->name('about');
Route::get('/contact',                    [App\Http\Controllers\PageController::class, 'contact'])->name('contact');
Route::get('/recipes',                [App\Http\Controllers\PageController::class, 'recipes'])->name('recipes');
Route::get('/recipe/preview/',    [App\Http\Controllers\PageController::class, 'recipe'])->name('recipe-preview');
Route::get('/profile',                [App\Http\Controllers\ProfileController::class, 'profile'])->name('profile');
Route::post('/profile/update',        [App\Http\Controllers\ProfileController::class, 'editProfile'])->name('update-profile');
Route::get('/all',                    [App\Http\Controllers\PageController::class, 'viewall'])->name('viewall');
Route::get('/category/{slug}',        [App\Http\Controllers\PageController::class, 'category'])->name('view-category');
Route::get('/product/{slug}',         [App\Http\Controllers\PageController::class, 'preview'])->name('show-product');
Route::get('/cash/checkout',          [App\Http\Controllers\CheckoutController::class, 'checkout'])->name('checkout');
Route::post('/store/order',           [App\Http\Controllers\CheckoutController::class, 'store'])->name('store.order');


Route::group(['prefix' => 'cart'], function () {
    Route::get('/', [CartController::class, 'index'])->name('cart.index');
    Route::post('/store/{product}', [CartController::class, 'store'])->name('cart.store');
    Route::patch('/update/{product}', [CartController::class, 'update'])->name('cart.update');
    Route::delete('/delete/{product}', [CartController::class, 'destroy'])->name('cart.destroy');
    Route::post('/switchToSaveForLater/{product}', [CartController::class, 'switchToSaveForLater'])->name('cart.switchToSaveForLater');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
