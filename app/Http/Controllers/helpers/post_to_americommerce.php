

<?php

function americ_store ($type,$data){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => ''.env('ac_base_uri').$type,
      CURLOPT_RETURNTRANSFER => true, 
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 3,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $data,
      CURLOPT_HTTPHEADER => array(
        'X-AC-Auth-Token'=>env('ac_token') ,
        'content-type: application/json',
        
      ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    $response = json_decode($response, TRUE);
    print_r ($response['id']);
}